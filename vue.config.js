module.exports = {
  publicPath: process.env.VUE_APP_PUBLIC_PATH,
  devServer: {
    host: '0.0.0.0',
    hot: true,
    disableHostCheck: true//,
    //proxy: 'https://image.ifremer.fr/'
  },
  configureWebpack: {
    devtool: 'source-map'
  }
}