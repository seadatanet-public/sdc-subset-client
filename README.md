# SDC-SUBSET-IFR
L'application sdc-subset-ifr à été développée dans le cadre de la VRE SeaDataCloud, cette application client permet de combiner plusieurs applications afin de réaliser le subsetting et la visualisation de données.

# Installation et implémentation
## Technologies : Framework, librairies
Ce projet utilise le framework VueJS 2 et a été généré avec vue-cli-service 3.8.0.  

[Bootstrap 4](https://bootstrap-vue.js.org/) pour VueJS permet la mise ne place rapide d'une UI responsive.  
[Leaflet](https://leafletjs.com/) pour la carthographie.  
[Highcharts](https://www.highcharts.com/) pour VueJS permet la mise ne place rapide et interactive de graphes de données.  

## GIT
La branche `master` doit être l'image des sources de l'application executée en production, aucun push/pull ne doit être réalisé depuis cette branche. Le workflow de branche par fonctionnalité est réalisé, c'est à dire que pour chaque nouvelle feature developpée une nouvelle branche est créée à partir de la branche `develop`.

## Setup du projet
Récupération des sources depuis la branche `develop` du projet GIT dans son IDE :
>  cd path-to-project  
>  git clone -b develop https://gitlab.ifremer.fr/seadatanet/vre/sdc-subset-client.git 

installation des dépendances du projet grâce à npm :
>  cd sdc-subset-client
>  npm install  

## Build application

Build de l'application pour déploiement dans différents environnements :  

docker developpement : `npm run build:docker`   
docker production : `npm run build:docker-prod`  
demonstration sur www.ifremer.fr : `npm run build`  

## Executer l'application en local

Run `npm run serve` for a dev server. Navigate to `http://localhost:8080/sdc-subset-ifr/`. The app will automatically reload if you change any of the source files.

## Déployer l'application sous docker

Réaliser un build et le déposer à l'emplacement requis par l'architecture docker suivante : https://gitlab.ifremer.fr/seadatanet/vre/ifr-sdn-subsetting.git  

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
