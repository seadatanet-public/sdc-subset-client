import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Configuration from './views/Configuration.vue'
import File from './views/File.vue'
import { store } from './store';

Vue.use(Router)

const router =  new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/conf',
      name: 'configuration',
      component: Configuration
    },
    {
      path: '/file',
      name: 'file-selector',
      component: File
    }

  ]
});

router.beforeEach((to, from, next) => {  
  if (to.fullPath === '/erddap') {    
    if (!store.state.tokenValide) {
      next('/file');
    }
  }
  if (to.fullPath === '/') {
    if (!store.state.tokenValide) {
      next('/file');
    }
  }
  next();
});

export default router;