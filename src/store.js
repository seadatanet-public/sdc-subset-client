/**
 * Shared data by identity. https://vuejs.org/v2/guide/state-management.html
 * Good solution for simple structure of shared data. If not prefere to use VUEX.
 */
export const store = {
  state: {
    tokenValide: false,
    configurationFail: false,
    accessDirectory: undefined,
    datasetId: 'SeaDataNet_inSitu',
    dataset_directory: '',
    subsetting: { selectedSubset: undefined, subsets: {} }
  },
  addSubset(key, data) {
    this.state.subsetting.subsets = {};
    this.state.subsetting.subsets[key] = data;
    this.state.subsetting.selectedSubset = key;
  },
  removeSubset(key) {
    delete this.state.subsetting.subsets[key];
  }
};