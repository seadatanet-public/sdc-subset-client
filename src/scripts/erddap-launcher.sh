#!/usr/bin/env bash

echo "HOSTNAME = $HOST_NAME"
echo "PORT = $PORT_NAME"
echo "Will start erddap. You will be able to access the webpage by reaching following URL :"
echo "http://$HOST_NAME:$PORT_NAME/erddap"
/opt/tomcat8/bin/catalina.sh run
