export const toast = {
  methods: {
    makeToast(title, message) {
      this.$bvToast.toast(message, {
        title: title,
        autoHideDelay: 5000,
        toaster: 'b-toaster-bottom-right',
        appendToast: true
      })
    }
  }
}